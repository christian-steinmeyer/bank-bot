import json
import logging

from bank_bot.comdirect.SimpleClient import SimpleClient
from bank_bot.comdirect.types import Credentials


def main():
    logging.basicConfig(level=logging.DEBUG)
    with open('credentials.json') as file:
        credentials = json.load(file)
    client = SimpleClient(Credentials(**credentials['comdirect']))
    client.fetch_data()
    client.print_status_update()
    client.print_all_depot_transactions()
    client.client.refresh_token()


if __name__ == '__main__':
    main()
