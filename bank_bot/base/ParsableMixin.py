from typing import Iterable


class ParsableMixin:
    @classmethod
    def from_collection(cls, collection: Iterable):
        result = []
        for item in collection:
            result.append(cls(**item))
        return result
