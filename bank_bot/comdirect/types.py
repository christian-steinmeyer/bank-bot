import datetime
from dataclasses import dataclass
from enum import Enum, auto

from bank_bot.base.ParsableMixin import ParsableMixin


@dataclass
class Credentials:
    client_id: str
    client_secret: str
    username: str
    password: str


@dataclass(init=False)
class Instrument:
    wkn: str
    isin: str
    name: str
    shortName: str

    def __init__(self, name: str, shortName: str, wkn: str, isin: str, **kwargs):
        self.name = name
        self.shortName = shortName
        self.wkn = wkn
        self.isin = isin


class ProductType(Enum):
    ACCOUNT = auto()
    CARD = auto()
    DEPOT = auto()


class TransactionType(Enum):
    BUY = auto()
    SELL = auto()


@dataclass(init=False)
class Product(ParsableMixin):
    targetClientId: str
    productId: str
    productType: ProductType

    def __init__(self, targetClientId: str, productId: str, productType: str, **kwargs):
        self.targetClientId = targetClientId
        self.productId = productId
        self.productType = ProductType[productType]


@dataclass(init=False)
class DepotTransaction(ParsableMixin):
    transactionType: TransactionType
    date: datetime.date
    quantity: float
    instrument: Instrument
    executionPrice: float
    unit: str
    value: float

    def __init__(self, transactionType: str, bookingDate: str, quantity: dict, instrument: dict, executionPrice: dict,
                 transactionValue: dict, **kwargs):
        self.transactionType = TransactionType[transactionType]
        self.date = datetime.date.fromisoformat(bookingDate)
        self.quantity = float(quantity['value'])
        self.instrument = Instrument(**instrument)
        self.executionPrice = float(executionPrice['value'])
        self.unit = executionPrice['unit']
        self.value = float(transactionValue['value'])

    def __str__(self):
        date = self.date
        transaction_type = self.transactionType.name
        quantity = self.quantity
        unit = self.unit
        instrument = f"{self.instrument.shortName} (WKN {self.instrument.wkn})"
        value = self.value
        price = self.executionPrice
        return f"{date}: {transaction_type} {instrument} for {value} ({quantity} * {price}) {unit}"
