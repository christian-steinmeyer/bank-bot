from typing import List

from comdirect_api.comdirect_client import ComdirectClient

from bank_bot.comdirect.types import Credentials, ProductType, DepotTransaction, Product


class SimpleClient:
    def __init__(self, credentials: Credentials):
        self.client_ids = set()
        self.products: List[Product] = []
        self.client = ComdirectClient(credentials.client_id, credentials.client_secret)
        self.client.fetch_tan(credentials.username, credentials.password)
        input("Press Enter once you've validated the push TAN request...")
        self.client.activate_session()

    def print_all_depot_transactions(self):
        for depot in self.get_depots():
            response = self.client.get_depot_transactions(depot.productId, max_booking_date="2010-01-01")
            transactions = DepotTransaction.from_collection(response['values'])
            for transaction in transactions:
                print(transaction)

    def fetch_data(self):
        report = self.client.get_report()
        total = float(report['aggregated']['balanceEUR']['value'])
        unit = report['aggregated']['balanceEUR']['unit']
        available = float(report['aggregated']['availableCashAmountEUR']['value'])
        print(f"Overall, you have {total} {unit} (and {available} {unit} available right now).")
        self.products = Product.from_collection(report['values'])

    def print_status_update(self):
        num_depots = len(self.get_depots())
        num_accounts = len(self.get_accounts())
        num_cards = len(self.get_cards())
        print(f"Found {num_accounts} accounts, {num_cards} cards, and {num_depots} depots.")

    def get_depots(self):
        return self._get_product_by_type(ProductType.DEPOT)

    def get_accounts(self):
        return self._get_product_by_type(ProductType.ACCOUNT)

    def get_cards(self):
        return self._get_product_by_type(ProductType.CARD)

    def _get_product_by_type(self, product_type):
        if len(self.products) < 1:
            self.fetch_data()
        return [product for product in self.products if product.productType == product_type]
