# Bank Bot
A small and simple library to automate some bank interaction via APIs.

# Usage
It's in a very early stage. 
For now, download and execute directly.
You'll need to rename `credentials-example.json` to `credentials.json`
and provide the necessary credentials.